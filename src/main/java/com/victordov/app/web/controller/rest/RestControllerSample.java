package com.victordov.app.web.controller.rest;

import com.victordov.app.web.dto.UserJson;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;

/**
 * Developed by vdovgaliuc, on 1/19/15.
 */
@RestController
//@RequestMapping("/api")
public class RestControllerSample {

    @RequestMapping(value = "/rest/user", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getUser() {
        UserJson userJson = new UserJson();
        userJson.setName("Victor");
        return new ResponseEntity<>(userJson, HttpStatus.OK);
    }

    @RequestMapping(value = "/api/rest/user/upload", method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<?> saveUser( UserJson ring) throws IOException {
//        System.out.println(ring);
//
//        ObjectMapper o = new ObjectMapper();
//        UserJson userJson = o.readValue(ring, UserJson.class);
//        System.out.println(userJson);
//        return new ResponseEntity<>(ring, HttpStatus.OK);

        return new ResponseEntity<Object>(HttpStatus.OK);
    }

}
