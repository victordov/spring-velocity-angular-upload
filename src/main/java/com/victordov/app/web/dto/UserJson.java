package com.victordov.app.web.dto;

import org.springframework.web.multipart.MultipartFile;

import java.io.Serializable;

/**
 * Developed by vdovgaliuc, on 1/19/15.
 */
public class UserJson implements Serializable {
    private String name;
    private MultipartFile file;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public MultipartFile getFile() {
        return file;
    }

    public void setFile(MultipartFile file) {
        this.file = file;
    }

    @Override
    public String toString() {
        return "UserJson{" +
                "name='" + name + '\'' +
                ", file=" + file +
                '}';
    }
}