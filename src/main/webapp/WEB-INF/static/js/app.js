(function () {
    'use strict';

    var dependencies = [
        'ngResource',
        'ngRoute',
        'toastr',
        'lr.upload'
    ];
    var app = angular.module('app', dependencies);


    app.controller('MyController', ['$scope', 'UserService', 'formDataObject', '$http', 'upload','$window',
        function ($scope, UserService, formDataObject, $http, upload,$window) {
        var mc = this;
        mc.user = {};

        UserService.getUser().then(function (data) {
            mc.user = data;
        });

        mc.upload = upload1;

        function upload3() {
            var fd = new FormData();
            fd.append("file", mc.user.file);
            fd.append("name", "TEST");
            var xhr = new XMLHttpRequest();
            xhr.open("POST", "/api/rest/user/upload");
            xhr.send(fd)
        }

        function upload2() {
            upload({
                url: '/api/rest/user/upload',
                method: 'POST',
                data: {name: mc.user.name, file: new FormData(mc.user.file)}
            }).then(
                function (response) {
                    console.log(response.data); // will output whatever you choose to return from the server on a successful upload
                },
                function (response) {
                    console.error(response); //  Will return if status code is above 200 and lower than 300, same as $http
                });
        }

        function upload1() {
            var usr = mc.user;
            console.log(JSON.stringify(usr));
            //console.log($window.atob(JSON.stringify(usr)));
            //return $http({
            //    method: 'POST',
            //    url: '/api/rest/user/upload',
            //    data: mc.user,
            //    transformRequest: formDataObject,
            //    headers: {'Content-Type': undefined}
            //});
            $http.post('/api/rest/user/upload', JSON.stringify(usr));

        }

    }]);

    app.factory('UserService', ['$http', function ($http) {
        return {
            getUser: function () {
                return $http.get('/api/rest/user').then(function (response) {
                    return response.data;
                })
            }
        }
    }]);

    app.factory('formDataObject', function () {
        return function (data) {
            var fd = new FormData();
            angular.forEach(data, function (value, key) {
                fd.append(key, value);
                console.log(key +" " + value);
            });
            return fd;
        };
    });


    app.config(function (toastrConfig) {
        angular.extend(toastrConfig, {
            allowHtml: true,
            closeButton: false,
            closeHtml: '<button>&times;</button>',
            containerId: 'toast-container',
            extendedTimeOut: 1000,
            iconClasses: {
                error: 'toast-error',
                info: 'toast-info',
                success: 'toast-success',
                warning: 'toast-warning'
            },
            messageClass: 'toast-message',
            positionClass: 'toast-bottom-right',
            tapToDismiss: true,
            timeOut: 4000,
            titleClass: 'toast-title',
            toastClass: 'toast'
        });
    });


    app.directive("fileread", [function () {
        return {
            scope: {
                fileread: "="
            },
            link: function (scope, element, attributes) {
                element.bind("change", function (changeEvent) {
                    var reader = new FileReader();
                    reader.onload = function (loadEvent) {
                        scope.$apply(function () {
                            scope.fileread = loadEvent.target.result;
                        });
                    }
                    reader.readAsDataURL(changeEvent.target.files[0]);
                });
            }
        }
    }]);
}());
